import java.util.Random;

public class OneBag implements IIndiv {
    String[] result;
    private Random random = new Random();
    private double profit;

    OneBag() {
        result = new String[DataFromFile.itemList.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextBoolean() ? "1" : "0";
        }
        this.evaluate();
    }

    OneBag(String[] result) {
        this.result = new String[DataFromFile.itemList.size()];
        System.arraycopy(result, 0, this.result, 0, this.result.length);
        this.evaluate();
    }

    OneBag(IIndiv iIndiv) {
        this.result = new String[DataFromFile.itemList.size()];
        System.arraycopy(iIndiv.getResult(), 0, this.result, 0, this.result.length);
        this.evaluate();
    }

    @Override
    public void setResult(int position, String value) {
        result[position] = value;
    }

    @Override
    public void mutation(double pm) {
//        if (random.nextDouble() > pm) {
//            int position = random.nextInt(result.length);
//            result[position] = result[position].equals("1") ? "0" : "1";
//        }

        for (int i = 0; i < result.length; i++) {
            if (random.nextDouble() < pm) {
                result[i] = result[i].equals("1") ? "0" : "1";
            }
        }
    }

    @Override
    public String evaluate() {
        profit = 0;
        int width = 0;

        for (int i = 0; i < result.length; i++) {
            if (result[i].equals("1")) {
                profit += DataFromFile.itemList.get(i).getProfit();
                width += DataFromFile.itemList.get(i).getWeight();
            }
        }
        if (Integer.parseInt(DataFromFile.maxWidth[0]) < width) {
            profit = 0;
        }
        return String.valueOf(width);
    }

    @Override
    public void crossover(IIndiv indiv, int position) {
        String[] helpResult = result;

        for (int i = position; i < result.length; i++) {
            result[i] = indiv.getResult()[i];
            indiv.setResult(i, helpResult[i]);
        }
    }

    @Override
    public IIndiv newObject() {
        return new OneBag(this.result);
    }

    @Override
    public void fix() {

    }

    @Override
    public double getProfit() {
        return profit;
    }

    @Override
    public String[] getResult() {
        return result;
    }
}
