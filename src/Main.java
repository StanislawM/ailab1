import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    private static int gen = 1000;

    public static void main(String[] args) throws FileNotFoundException {
        Results results = new Results();
        Algorithm algorythm = new Algorithm(100, 0.7, 0.05, 10, results);

        DataFromFile dataFromFile = new DataFromFile();
        dataFromFile.getDataMultiBag();

        long start = System.nanoTime();
        algorythm.initializeMultiBag();

        for (int i = 0; i < gen; i++) {
            algorythm.nextGeneration(algorythm.generation);
        }
        long stop = System.nanoTime();
        System.out.println("czas trwania: " + (((double) stop - (double) start) / 1000000000));
        System.out.println();

        try {
            algorythm.printResults();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
