import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Generation {
    private List<IIndiv> indivList = new ArrayList<>();
    private Random random = new Random();
    private int popSize;

    Generation(int popSize) {
        this.popSize = popSize;
    }

    private Generation(List<IIndiv> list, int popSize) {
        indivList = new ArrayList<>(list);
        this.popSize = popSize;
    }

    void initializeOneBag(int popSize) {
        this.popSize = popSize;
        for (int i = 0; i < popSize; i++) {
            indivList.add(new OneBag());
        }
    }

    void initializeMultiBag(int popSize) {
        this.popSize = popSize;
        for (int i = 0; i < popSize; i++) {
            indivList.add(new MultiBag());
        }
    }

    Generation selection(Generation generation, int tour) {
        List<IIndiv> newIndivList = new ArrayList<>();
        for (int i = 0; i < popSize; i++) {
            newIndivList.add(selectNextIndiv(generation, tour));
        }
        return new Generation(newIndivList, popSize);
    }

    private IIndiv selectNextIndiv(Generation generation, int tour) {
        IIndiv newIndiv = generation.indivList.get(random.nextInt(popSize));
        for (int i = 0; i < tour; i++) {
            IIndiv indiv = generation.indivList.get(random.nextInt(popSize));
            newIndiv = newIndiv.getProfit() < indiv.getProfit() ? indiv : newIndiv;
        }
        return newIndiv.newObject();
    }

    Generation crossover(Generation generation, double px) {
        List<IIndiv> newIndivList = new ArrayList<>();
        while (newIndivList.size() < popSize) {
            IIndiv indiv1 = generation.indivList.get(random.nextInt(popSize));
            IIndiv indiv2 = generation.indivList.get(random.nextInt(popSize));
            if (random.nextDouble() < px) {
                int length = indiv1.getResult().length;
                int position = random.nextInt(length);
                IIndiv help = indiv1.newObject();
                indiv1.crossover(indiv2, position);
                indiv2.crossover(help, position);
            }
            newIndivList.add(indiv1.newObject());
            newIndivList.add(indiv2.newObject());
        }
        this.indivList.forEach(IIndiv::evaluate);
        return new Generation(newIndivList, popSize);
    }

    Generation mutation(Generation generation, double pm) {
        List<IIndiv> newIndivList = new ArrayList<>();
        for (int i = 0; i < popSize; i++) {
            IIndiv indiv = generation.indivList.get(i);
            indiv.mutation(pm);
            newIndivList.add(indiv.newObject());
        }
        return new Generation(newIndivList, popSize);
    }

    void evaluate(Results results) {
        double avgProfit = 0;
        IIndiv low = indivList.get(0);
        IIndiv best = indivList.get(0);
        for (IIndiv x : indivList) {
            best = best.getProfit() < x.getProfit() ? x : best;
            low = low.getProfit() > x.getProfit() ? x : low;
            avgProfit += x.getProfit();
        }
        avgProfit = avgProfit / indivList.size();
        results.bestProfit.add(best.getProfit());
        results.avgProfit.add(avgProfit);
        results.lowProfit.add(low.getProfit());
        results.bestResult.add(best.newObject());
        results.lowResult.add(low.newObject());
    }
}
