public interface IIndiv {

    double getProfit();

    String[] getResult();

    void setResult(int position, String value);

    void mutation(double pm);

    String evaluate();

    void crossover(IIndiv indiv, int position);

    IIndiv newObject();

    void fix();
}
