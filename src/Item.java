import java.util.Comparator;

class Item implements Comparator<Item> {
    private int id;
    private int profit;
    private int weight;

    Item(int id, int profit, int weight) {
        this.id = id;
        this.profit = profit;
        this.weight = weight;
    }

    @Override
    public int compare(Item o1, Item o2) {
        return o1.profit - o2.profit;
    }


    int getWeight() {
        return weight;
    }

    int getProfit() {
        return profit;
    }
}
