import java.util.Arrays;
import java.util.Random;

public class MultiBag implements IIndiv {
    private String[] result;
    private Random random = new Random();
    private double profit;

    MultiBag() {
        result = new String[DataFromFile.itemList.size() * DataFromFile.numberOfBags];
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextBoolean() ? "1" : "0";
        }
        fix();
    }

    private MultiBag(String[] result) {
        this.result = new String[DataFromFile.itemList.size() * DataFromFile.numberOfBags];
        System.arraycopy(result, 0, this.result, 0, this.result.length);
        fix();
    }

    public MultiBag(IIndiv result) {
        this.result = new String[DataFromFile.itemList.size() * DataFromFile.numberOfBags];
        System.arraycopy(result.getResult(), 0, this.result, 0, this.result.length);
        fix();
    }

    @Override
    public void setResult(int position, String value) {
        result[position] = value;
    }

    @Override
    public void mutation(double pm) {
        for (int i = 0; i < result.length; i++) {
            if (random.nextDouble() < pm) {
                result[i] = result[i].equals("1") ? "0" : "1";
            }
        }
        fix();
    }

    @Override
    public String evaluate() {
        profit = 0;
        int[] width = new int[DataFromFile.numberOfBags];

        for (int i = 0; i < DataFromFile.itemList.size(); i++) {
            for (int j = 0; j < DataFromFile.numberOfBags; j++) {
                int i1 = i + j * DataFromFile.itemList.size();
                if (result[i1].equals("1")) {
                    profit += DataFromFile.itemList.get(i).getProfit();
                    width[j] += DataFromFile.itemList.get(i).getWeight();
                }
            }
        }

        for (int i = 0; i < width.length; i++) {
            if (Integer.parseInt(DataFromFile.maxWidth[i]) < width[i]) {
                profit = 0;
            }
        }
        int sumWidth = 0;
        int sumMaxWidth = 0;
        for (int i = 0; i < width.length; i++) {
            sumWidth += width[i];
            sumMaxWidth += Integer.parseInt(DataFromFile.maxWidth[i]);
        }
        if (sumMaxWidth < sumWidth) {
            profit = 0;
        }

        return Arrays.toString(width);
    }

    @Override
    public void crossover(IIndiv indiv, int position) {
        IIndiv helpIndiv = new MultiBag(result);

        for (int i = position; i < result.length; i++) {
            result[i] = indiv.getResult()[i];
            indiv.setResult(i, helpIndiv.getResult()[i]);
        }
        fix();
        indiv.fix();
    }

    @Override
    public IIndiv newObject() {
        return new MultiBag(this.result);
    }

    @Override
    public double getProfit() {
        return profit;
    }

    @Override
    public String[] getResult() {
        return result;
    }

    public void fix() {
        for (int i = 0; i < DataFromFile.itemList.size(); i++) {
            boolean isItemUsed = false;
            int position = 0;
            for (int j = 0; j < DataFromFile.numberOfBags; j++) {
                if (isItemUsed) {
                    result[position + i] = "0";
                }
                if (result[position + i].equals("1")) {
                    isItemUsed = true;
                }
                position += result.length / DataFromFile.numberOfBags;
            }
            isItemUsed = false;
        }
        evaluate();
    }
}
