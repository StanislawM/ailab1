import java.util.ArrayList;
import java.util.List;

class Results {
    List<Double> avgProfit = new ArrayList<>();
    List<Double> lowProfit = new ArrayList<>();
    List<Double> bestProfit = new ArrayList<>();
    List<IIndiv> bestResult = new ArrayList<>();
    List<IIndiv> lowResult = new ArrayList<>();
}
