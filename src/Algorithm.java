import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

class Algorithm {
    Generation generation;
    private Results results;
    private int popSize;
    private double px;
    private double pm;
    private int tour;
    private Double iIndiv;

    Algorithm(int popSize, double px, double pm, int tour, Results results) {
        this.popSize = popSize;
        this.px = px;
        this.pm = pm;
        this.tour = tour;
        this.generation = new Generation(popSize);
        this.results = results;
    }

    void initializeMultiBag() {
        generation.initializeMultiBag(popSize);
    }

    void initializeOneBag() {
        generation.initializeOneBag(popSize);
    }

    void nextGeneration(Generation generation) {
        this.generation = generation.selection(generation, tour);
        this.generation = generation.crossover(this.generation, px);
        this.generation = generation.mutation(this.generation, pm);
        this.generation.evaluate(results);
    }

    void printResults() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("profit.csv"));
        for (int i = 0; i < results.bestProfit.size(); i++) {
            writer.write(results.bestProfit.get(i) + "\t" + results.avgProfit.get(i) + "\t" + results.lowProfit.get(i) + "\n");
        }
        results.bestResult.sort((iIndiv1, t1) -> (int) (t1.getProfit() - iIndiv1.getProfit()));
        results.lowResult.sort((iIndiv1, t1) -> (int) (t1.getProfit() - iIndiv1.getProfit()));
        System.out.println(results.bestResult.get(0).getProfit());
        System.out.println(results.bestResult.get(0).evaluate());
        System.out.println(results.bestResult.get(0).getProfit());
        for (int i = 0; i < DataFromFile.numberOfBags; i++) {
            String[] result = results.bestResult.get(0).getResult();
            int i1 = i * DataFromFile.itemList.size();
            System.out.print(Arrays.asList(result).subList(i1, i1 + DataFromFile.itemList.size()));
        }
        System.out.println();
        System.out.println();
        for (String[] s : DataFromFile.resultList) {
            System.out.print(Arrays.toString(s));
        }

        writer.close();
    }
}
