import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class DataFromFile {
    static String[] maxWidth;
    static ArrayList<Item> itemList = new ArrayList<Item>();
    static ArrayList<String[]> resultList = new ArrayList<String[]>();
    static int numberOfBags;

    void getDataMultiBag() throws FileNotFoundException {
        Scanner odczyt = new Scanner(new File("./multiBag/p01.csv"));

        String[] s = odczyt.nextLine().split(",");
        numberOfBags = Integer.parseInt(s[0]);
        int numberOfItems = Integer.parseInt(s[1]);

        maxWidth = odczyt.nextLine().split(",");

        for (int i = 0; i < numberOfItems; i++) {
            s = odczyt.nextLine().split(",");
            itemList.add(new Item(i, Integer.parseInt(s[0]), Integer.parseInt(s[1])));
        }

        for (int i = 0; i < numberOfBags; i++) {
            resultList.add(odczyt.nextLine().split(","));
        }
    }

    void getDataOneBag() throws FileNotFoundException {
        Scanner odczyt = new Scanner(new File("./oneBag/p01.csv"));
        numberOfBags = 1;

        String[] s = odczyt.nextLine().split(",");
        int numberOfIntem = Integer.parseInt(s[0]);
        maxWidth = new String[1];
        maxWidth[0] = s[1];

        for (int i = 0; i < numberOfIntem; i++) {
            s = odczyt.nextLine().split(",");
            itemList.add(new Item(i, Integer.parseInt(s[0]), Integer.parseInt(s[1])));
        }

        resultList.add(odczyt.nextLine().split(","));
    }
}
